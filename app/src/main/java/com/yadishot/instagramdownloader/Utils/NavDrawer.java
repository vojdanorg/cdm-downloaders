package com.yadishot.instagramdownloader.Utils;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.widget.Toolbar;

public class NavDrawer {
    private Activity activity;
    private Context context;
    private Toolbar toolbar;

    public NavDrawer(Activity activity, Context context, Toolbar toolbar) {
        this.activity = activity;
        this.context = context;
        this.toolbar = toolbar;
    }
}
