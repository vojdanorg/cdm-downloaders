package com.yadishot.instagramdownloader.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.snatik.storage.Storage;
import com.yadishot.instagramdownloader.R;

import java.io.File;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import yogesh.firzen.mukkiasevaigal.D;
import yogesh.firzen.mukkiasevaigal.F;

public class AdapterDownloads extends RecyclerView.Adapter<AdapterDownloads.viewHolder> {

    List<File> files;
    Context context;

    public AdapterDownloads(List<File> files, Context context) {
        this.files = files;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_downloads, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        public ImageView ic_downloads;
        public TextView fileName, file_info;
        public viewHolder(@NonNull View itemView) {
            super(itemView);
            ic_downloads = itemView.findViewById(R.id.ic_downloads);
            file_info = itemView.findViewById(R.id.file_info);
            fileName = itemView.findViewById(R.id.fileName);
        }

        public void bind() {
            fileName.setText(files.get(getAdapterPosition()).getName());

            // get file last modified

            File file = new File(files.get(getAdapterPosition()).getPath());
            Date lastModified = new Date(file.lastModified());

            // get kilo byte size of file
            long fileKbSize = files.get(getAdapterPosition()).length()/1024;
            // get mega byte size of file
            long fileMbSize = fileKbSize/1024;
            // set mega byte sizes to files
            file_info.setText(fileMbSize + " Mb | "+lastModified);

            if (fileMbSize <= 0){
                file_info.setText(context.getString(R.string.no_size_of_file) +" | "+lastModified);
            }
            // switch to icons for images and music and videos
            if (files.get(getAdapterPosition()).getName().endsWith(".jpg")){
                ic_downloads.setImageResource(R.drawable.ic_photo);

            }else if (files.get(getAdapterPosition()).getName().endsWith(".mp3")){
                ic_downloads.setImageResource(R.drawable.ic_audiotrack);
            }else if (files.get(getAdapterPosition()).getName().endsWith(".mp4")){
                ic_downloads.setImageResource(R.drawable.ic_vide);
            }

        }
    }
}
