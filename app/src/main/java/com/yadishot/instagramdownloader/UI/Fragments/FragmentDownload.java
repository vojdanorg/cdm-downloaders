package com.yadishot.instagramdownloader.UI.Fragments;


import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.FileUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.snatik.storage.Storage;
import com.yadishot.instagramdownloader.Adapters.AdapterDownloads;
import com.yadishot.instagramdownloader.R;
import com.yadishot.instagramdownloader.UI.Activity.MainActivity;
import com.yadishot.instagramdownloader.Utils.DownloaderInit;
import com.yadishot.instagramdownloader.Utils.Tools;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import br.com.goncalves.pugnotification.notification.PugNotification;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentDownload extends Fragment {

    private static final String CHANNGEL_ID = "1001";

    @BindView(R.id.recyclerView_dl)
    RecyclerView recyclerViewDl;

    AdapterDownloads adapterDownloads;

    Storage storage;
    @BindView(R.id.top_menu)
    RelativeLayout topMenu;
    @BindView(R.id.percent)
    TextView percent;
    @BindView(R.id.amounts)
    TextView amounts;
    @BindView(R.id.download_amount)
    MaterialProgressBar downloadAmount;
    @BindView(R.id.topDownloadMenu)
    RelativeLayout topDownloadMenu;
    @BindView(R.id.txt_download_files)
    TextView txtDownloadFiles;
    @BindView(R.id.add_download_link)
    FloatingActionButton addDownloadLink;

    public FragmentDownload() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_download, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        storage = new Storage(getContext());
        initPrDownloader();
        initFile();

    }

    private void initPrDownloader() {
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getContext(), config);
    }

    private void initFile() {
        String path = String.valueOf(Tools.appDirectory());
        recyclerViewDl.setLayoutManager(new LinearLayoutManager(getContext()));
        List<File> files = storage.getFiles(path);
        adapterDownloads = new AdapterDownloads(files, getContext());
        recyclerViewDl.setAdapter(adapterDownloads);
    }

    @OnClick(R.id.add_download_link)
    void getToDownload(View view) {
        alertDialogDownload();
    }

    private void alertDialogDownload() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_alert_dialog_download, null);
        builder.setView(view);

        EditText link_download = view.findViewById(R.id.link_download);
        EditText set_file_name = view.findViewById(R.id.set_file_name);
        Button btnStartDownload = view.findViewById(R.id.start_download);
        Button btnCancelDownload = view.findViewById(R.id.cancel_download);

        AlertDialog b = builder.create();
        b.show();

        btnCancelDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        btnStartDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String linkDownload = link_download.getText().toString().trim();
                String file_name = set_file_name.getText().toString().trim();
                if (linkDownload.isEmpty()) {
                    Toast.makeText(getContext(), "download link required!", Toast.LENGTH_SHORT).show();
                } else if (file_name.isEmpty()) {
                    Toast.makeText(getContext(), "file name required!", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        URL url = new URL(linkDownload);
                        Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    startDownload(link_download.getText().toString(), set_file_name.getText().toString());
                    b.dismiss();
                }
            }
        });

    }

    private void startDownload(String link, String file_name) {
        downloadAmount.setVisibility(View.GONE);
            String path = String.valueOf(Tools.appDirectory());
            DownloaderInit downloaderInit = new DownloaderInit();
            downloaderInit.DownloaderInits(getContext(), link, path, Tools.getFileNameFromURL(link), percent, downloadAmount, addDownloadLink, adapterDownloads);
        notificationInit();
    }

    private void notificationInit() {


    }

}
